# X deb

A private deb repository.

This repository provides the latest Debian packages:

- adguard-home
- bat
- cargo-deb
- clash, clash-geoip
- codium
- curl
- dishus
- fd
- golang
- goproxy
- hexyl
- htop
- hyperfine
- keepassxc
- mdbook
- nfpm
- shadowsocks-libev
- shadowsocks-rust
- tmux-mem-cpu-load
- v2ray
- v2ray-plugin

## Usage

1. Set up the APT Key

   Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/ubuntu/pubkey.gpg

   or (if you prefer key file in text format)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/ubuntu/pubkey.asc

2. Set up `sources.list`

   Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list` with the
   content (Ubuntu 24.04 for example):

        deb https://xdeb.gitlab.io/ubuntu noble universe

   The available source list files are:

   * Ubuntu 20.04 LTS (Focal Fossa)

         deb https://xdeb.gitlab.io/ubuntu focal universe   # NOT TESTED

   * Ubuntu 22.04 LTS (Jammy Jellyfish)

         deb https://xdeb.gitlab.io/ubuntu jammy universe

   * Ubuntu 24.04 LTS (Noble Numbat)

         deb https://xdeb.gitlab.io/ubuntu noble universe
